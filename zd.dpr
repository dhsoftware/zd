library zd;

{$E dmx}  // dmx is the extension for DCAL Dll's
{$R *.res}

uses
  UDCLibrary in '..\..\DCAL for Delphi Header Files\17\UDCLibrary.pas',
  UInterfaces in '..\..\DCAL for Delphi Header Files\17\UInterfaces.pas',
  UInterfacesRecords in '..\..\DCAL for Delphi Header Files\17\UInterfacesRecords.pas',
  URecords in '..\..\DCAL for Delphi Header Files\17\URecords.pas',
  UVariables in '..\..\DCAL for Delphi Header Files\17\UVariables.pas',
  UConstants in '..\..\DCAL for Delphi Header Files\17\UConstants.pas';

function Main(dcalstate: asint; act: action; pl, pargs: Pointer): wantType; stdcall;
var
  s : shortstring;
begin
  cvdisst (zhite-zbase, s);
  s := 'zD=' + s;
  wrterr (s, true);
  Result := XDone; { Necessary }
end;

exports
  Main; { This is the entry function that will be called by DataCAD. }

begin

end.
